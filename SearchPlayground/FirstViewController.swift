import UIKit
import SnapKit

class FirstViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UIScrollViewDelegate, UISearchControllerDelegate {

    let tableView = UITableView()
    let searchController = UISearchController(searchResultsController: nil)

    let searchTableView = UITableView()

    var searchControllerPresented = false

    override func viewDidLoad() {
        super.viewDidLoad()

        title = "First Max"
        view.backgroundColor = .white



        definesPresentationContext = true

        navigationItem.searchController = searchController
        navigationItem.hidesSearchBarWhenScrolling = false

//        searchController.dimsBackgroundDuringPresentation = false
        searchController.delegate = self



        view.addSubview(tableView)
        tableView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }

        tableView.delegate = self
        tableView.dataSource = self

        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 44

        tableView.register(UINib(nibName: String(describing: ChannelCell.self), bundle: nil), forCellReuseIdentifier: String(describing: ChannelCell.self))



        searchController.view.addSubview(searchTableView)
        searchTableView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }

        searchTableView.delegate = self
        searchTableView.dataSource = self

        searchTableView.rowHeight = UITableViewAutomaticDimension
        searchTableView.estimatedRowHeight = 44

        searchTableView.register(UINib(nibName: String(describing: ChannelCell.self), bundle: nil), forCellReuseIdentifier: String(describing: ChannelCell.self))
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        if let indexPath = tableView.indexPathForSelectedRow {
            tableView.deselectRow(at: indexPath, animated: true)
        }
        if let indexPath = searchTableView.indexPathForSelectedRow {
            searchTableView.deselectRow(at: indexPath, animated: true)
        }
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 50
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return tableView.dequeueReusableCell(withIdentifier: String(describing: ChannelCell.self)) ?? UITableViewCell(frame: .zero)
    }

    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if let cell = cell as? ChannelCell {
            cell.bind(text: "\(tableView === searchTableView ? "Search " : "")Channel \(indexPath.row)")
        }
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let controller = SecondViewController()
        controller.hidesBottomBarWhenPushed = true
        navigationController?.pushViewController(controller, animated: true)
    }

    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if searchController.isActive && searchControllerPresented && searchTableView.isDragging && !searchTableView.isDecelerating {
            searchController.searchBar.endEditing(true)
        }
    }

    func didPresentSearchController(_ searchController: UISearchController) {
        searchControllerPresented = true
    }

    func willDismissSearchController(_ searchController: UISearchController) {
        searchControllerPresented = false

        searchController.transitionCoordinator?.animate(alongsideTransition: { _ in
            self.searchTableView.alpha = 0
        })
    }

    func willPresentSearchController(_ searchController: UISearchController) {
        searchTableView.alpha = 0

        DispatchQueue.main.async {
            self.searchController.transitionCoordinator?.animate(alongsideTransition: { _ in
                self.searchTableView.alpha = 1
            })
        }
    }

}
