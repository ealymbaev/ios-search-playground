import UIKit
import SnapKit

class SecondViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    let tableView = UITableView()
    let searchController = UISearchController(searchResultsController: nil)

    override func viewDidLoad() {
        super.viewDidLoad()

        title = "Second Max"
        view.backgroundColor = .white



//        definesPresentationContext = true

//        navigationItem.searchController = searchController
//        navigationItem.hidesSearchBarWhenScrolling = false

//        searchController.dimsBackgroundDuringPresentation = false



        view.addSubview(tableView)
        tableView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }

        tableView.delegate = self
        tableView.dataSource = self

        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 44

        tableView.register(UINib(nibName: String(describing: ChannelCell.self), bundle: nil), forCellReuseIdentifier: String(describing: ChannelCell.self))
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 50
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return tableView.dequeueReusableCell(withIdentifier: String(describing: ChannelCell.self)) ?? UITableViewCell(frame: .zero)
    }

    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if let cell = cell as? ChannelCell {
            cell.bind(text: "Second Channel \(indexPath.row)")
        }
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }

}
