import Foundation
import UIKit

class MainController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()

        let firstController = FirstViewController()
        let firstNavigation = UINavigationController(rootViewController: firstController)
        firstNavigation.tabBarItem.title = "First Max"
        firstNavigation.tabBarItem.image = UIImage(named: "first")

        let secondController = SecondViewController()
        let secondNavigation = UINavigationController(rootViewController: secondController)
        secondNavigation.tabBarItem.title = "Second Max"
        secondNavigation.tabBarItem.image = UIImage(named: "second")

        viewControllers = [firstNavigation, secondNavigation]
    }

}
