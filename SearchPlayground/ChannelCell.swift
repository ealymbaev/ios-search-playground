import UIKit

class ChannelCell: UITableViewCell {

    @IBOutlet weak var label: UILabel?

    func bind(text: String) {
        label?.text = text
    }

}
